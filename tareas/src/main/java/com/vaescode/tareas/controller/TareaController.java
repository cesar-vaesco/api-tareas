package com.vaescode.tareas.controller;


import com.vaescode.tareas.model.Tarea;
import com.vaescode.tareas.repositorio.TareaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/tareas")
public class TareaController {

    @Autowired
    private TareaRepository tareaRepository;


    @GetMapping("")
    List<Tarea> index(){
        return tareaRepository.findAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    Tarea create(@RequestBody Tarea tarea){
        return tareaRepository.save(tarea);
    }

    @PutMapping("{id}")
    Tarea update(@PathVariable String id, @RequestBody Tarea tarea){
        Tarea tareaDB = tareaRepository.findById(id).orElseThrow( RuntimeException::new);

        tareaDB.setNombre(tarea.getNombre());
        tareaDB.setCompletado(tarea.isCompletado());

        return tareaRepository.save(tareaDB);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    void delete(@PathVariable String id){
        tareaRepository.deleteById(id);
    }


}
