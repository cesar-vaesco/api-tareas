# API-Tareas

 Aplicación fue desarrollada con Spring Boot(api rest), Angular(front-end) y MongoDB

## Api rest

- Creada con lenguaje Java usando el framework Spring configurado con Maven

### Dependecias utilizadas

- Spring Web
- Spring Data MongoDB
- Lombok

## Base de datos

- Para persiitir los datos se uso MongoDB de manera local

## Front End

- Para visualizar/listar, crear, modificar y eliminar las tareas se uso el framework Angular

<br>

<p align="center">
  <img src="tareascliente/src/assets/img/tareas.png" width="800" height="400"title="tareas">
</p>
